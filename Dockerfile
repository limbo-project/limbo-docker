FROM debian:stable-20200224-slim

LABEL maintainer="wenige@infai.org"

RUN apt-get update \
  && apt-get upgrade -y \
  && mkdir -p /usr/share/man/man1mkdir -p /usr/share/man/man1 \
  && apt-get install -y make unzip jq maven bc dpkg sudo curl git raptor2-utils cargo gfortran redis\
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
  && git clone https://github.com/SmartDataAnalytics/SparqlIntegrate.git \
  && cd SparqlIntegrate \
  && git checkout a200d8b \
  && mvn -U clean install -DskipTests=true \  
  && chmod +x reinstall-debs.sh \ 
  && ./reinstall-debs.sh

# Install Corporate Memory Control (cmemc) -> https://eccenca.com/go/cmemc
ARG CMEMC_VERSION=20.03.1
ADD https://releases.eccenca.com/cmemc/cmemc-v${CMEMC_VERSION}.zip /tmp/
ADD cmemc.ini /root/.config/cmemc/config.ini
ENV CMEMC_CONNECTION=portal.limbo-project.org
RUN cd /tmp \
  && unzip cmemc-v${CMEMC_VERSION}.zip \
  && cp cmemc-v${CMEMC_VERSION}/cmemc /usr/local/bin/cmemc \
  && rm -r -f /tmp/cmemc*

WORKDIR /data

ENTRYPOINT ["/bin/bash", "-c"]

