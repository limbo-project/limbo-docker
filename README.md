# LIMBO project docker base image

## Content

### SparqlIntegrate

...

### cmemc - eccenca Corporate Memory Command line interface

see https://eccenca.com/go/cmemc

```
docker run -i -t --rm aksw/limbo-base:latest cmemc
```

you can also use the official images directly:

see https://documentation.eccenca.com/latest/automate/cmemc-command-line-interface/special-topic-using-the-cmemc-docker-image
