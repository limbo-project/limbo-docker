export CONTAINER_NAME=aksw/limbo-base
export KILLTIMEOUT=10
TIME_STAMP=$(shell date --iso-8601=seconds)
# git investigation
export GITDESCRIBE=$(shell git describe --always --dirty)
export IMAGE_NAME=${CONTAINER_NAME}:${GITDESCRIBE}
export DOCKER_CMD?=docker

.DEFAULT_GOAL := help

## build the image
build:
	$(DOCKER_CMD) build -t ${IMAGE_NAME} .

build-latest:
	docker build -t ${CONTAINER_NAME}:latest .

push-latest:
	docker push ${CONTAINER_NAME}:latest

## push image with git describe tag
push:
	docker push ${CONTAINER_NAME}:${GITDESCRIBE}

## build the image based on Dockerfile (pull base images and ignore cache)
clean-build:
	$(DOCKER_CMD) build --pull=true --no-cache -t ${IMAGE_NAME}

## inspect the image by starting a shell session in a self-destructing container
shell:
	$(DOCKER_CMD) run -i -t --rm ${IMAGE_NAME} bash

## save a local image to a tar file
save:
	$(DOCKER_CMD) save -o ${CONTAINER_NAME}-${GITDESCRIBE}.tar ${IMAGE_NAME}

## show this help screen
help:
	@printf "Available targets\n\n"
	@awk '/^[a-zA-Z\-_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")-1); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "%-15s %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)
